//
//  AWSyncManager.h
//  testAnyWay
//
//  Created by Admin on 07.04.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "AWDataManager.h"



@interface AWSyncManager : NSObject


+(AWSyncManager *)shared;

-(NSDictionary*)apiSearchCity:(NSString*)searchString;
-(NSDictionary*)apiGetRequestID:(NSString*)requestString;
-(NSNumber *)apiGetProgress;
-(NSArray*)apiGetListRequest;
-(NSArray*)apiGetListAirLines;

@end
