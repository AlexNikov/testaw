//
//  AWSyncManager.m
//  testAnyWay
//
//  Created by Admin on 07.04.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "AWSyncManager.h"

@implementation AWSyncManager

+(AWSyncManager *)shared {
    
    static AWSyncManager  * instance;
    
    @synchronized(self) {
        if(!instance) {
            instance = [[AWSyncManager alloc] init];
            
        }
    }
    
    return instance;
}


#pragma mark -
#pragma mark API Commands

-(NSDictionary*)apiSearchCity:(NSString*)searchString{
    
    NSString * lng=[[NSUserDefaults standardUserDefaults] objectForKey:@"searchLng"];

    NSString * command=[NSString stringWithFormat:@"https://www.anywayanyday.com/AirportNames/?language=%@&filter=%@&_Serialize=JSON",lng,[searchString lowercaseString]];
    
    NSDictionary * reciveData=[self apiMethod:command];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"updateSearchList"
                                                            object:nil];
    });
    
    
    return reciveData;
    
}

-(NSDictionary*)apiGetRequestID:(NSString*)requestString{
    
    NSString * command=[NSString stringWithFormat:@"https://www.anywayanyday.com/api2/NewRequest2/?Route=%@&_Serialize=JSON",requestString];
    
    NSDictionary * reciveData=[self apiMethod:command];
    
    id temp=reciveData[@"Error"];
    
    if (reciveData!=nil && reciveData[@"Error"]== [NSNull null]) {
        [[AWDataManager shared] setRequestID:reciveData[@"IdSynonym"]];
    }
    
    return reciveData;
    
}

-(NSNumber *)apiGetProgress{
    
    NSString * command=[NSString stringWithFormat:@"https://www.anywayanyday.com/api2/RequestState/?R=%@&_Serialize=JSON",[[AWDataManager shared] requestID]];
    
    NSDictionary * reciveData=[self apiMethod:command];
    
    
    if (reciveData!=nil && reciveData[@"Error"]== [NSNull null]) {
        NSLog(@"complete=%@",reciveData[@"Completed"]);
        return reciveData[@"Completed"];
    }
    
    return nil;
    
}

-(NSArray*)apiGetListRequest{
    NSString * lng=[[NSUserDefaults standardUserDefaults] objectForKey:@"searchLng"];
    
        NSString * command=[NSString stringWithFormat:@"https://www.anywayanyday.com/api2/Fares2/?L=%@&C=RUB&DebugFullNames=true&_Serialize=JSON&R=%@",lng,[[AWDataManager shared] requestID]];
    
    NSDictionary * reciveData=[self apiMethod:command];
    
    if (reciveData!=nil && reciveData[@"Error"]== [NSNull null]) {
        return reciveData[@"Airlines"];
    }
    
    return nil;
}

-(NSArray*)apiGetListAirLines{
    NSString * command=[NSString stringWithFormat:@"https://www.anywayanyday.com/Controller/UserFuncs/BackOffice/GetAirlines/"];
    
    NSArray * reciveData=[self apiMethod:command];
    
    if (reciveData!=nil )
        return reciveData;
    
    
    return nil;
}

-(id) apiMethod:(NSString*) command
{
    
    NSURL * url=[NSURL URLWithString:[command stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
    
    
    NSMutableURLRequest * request = [[NSMutableURLRequest alloc] initWithURL: url
                                                                 cachePolicy: NSURLRequestReloadIgnoringLocalCacheData
                                                             timeoutInterval: 600
                                     ];
    
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    
    
    NSHTTPURLResponse * response;
    NSError * error;
    __weak NSData * respData = [NSURLConnection sendSynchronousRequest:request returningResponse:&response error:&error];
    
    if (error == nil)
    {
        NSDictionary * recive = [NSJSONSerialization JSONObjectWithData:respData options:0 error:&error];
        
        [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
        
        NSDictionary * answer = @{
                                  @"recive": (recive)?recive:@{},
                                  @"response": (response)?[NSString stringWithFormat:@"%li",(long)[response statusCode]]:@"NULL",
                                  @"error":(error)?error:@"NULL",
                                  @"url":[request URL]
                                  };
        
        if([response statusCode] == 200 && answer[@"recive"])
            return answer[@"recive"];
    }

    
    
    
    return nil;
    
}

@end
