//
//  AWListRequestTV.h
//  testAnyWay
//
//  Created by Admin on 08.04.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RNActivityView.h"
#import "UIView+RNActivityView.h"
#import "AWSyncManager.h"
#import "AWAirCell.h"

@interface AWListRequestTV : UIViewController<UITableViewDelegate,UITableViewDataSource>
{
    NSArray * listRequest;
    NSArray * Farce;
    NSArray * airlineNames;
}

@property (weak, nonatomic) IBOutlet UITableView *airTable;
@property (weak, nonatomic) IBOutlet UITableView *faresTable;


@property (weak, nonatomic) IBOutlet UILabel *lblFaresCell;



@end
