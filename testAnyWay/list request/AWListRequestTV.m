//
//  AWListRequestTV.m
//  testAnyWay
//
//  Created by Admin on 08.04.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "AWListRequestTV.h"

@interface AWListRequestTV ()

@end

@implementation AWListRequestTV

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.airTable.layer.cornerRadius=10;
    self.airTable.layer.masksToBounds=YES;
    self.airTable.tableHeaderView.hidden=YES;
    
    self.faresTable.layer.cornerRadius=10;
    self.faresTable.layer.masksToBounds=YES;
    self.faresTable.tableHeaderView.hidden=YES;
    
    airlineNames=[[AWSyncManager shared] apiGetListAirLines];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewDidAppear:(BOOL)animated{
    
        [self.navigationController.view showActivityViewWithMode:RNActivityViewModeAnnularDeterminate label:@"Loading" detailLabel:nil whileExecuting:@selector(myProgressTask) onTarget:self];


}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.airTable==tableView)
        return [listRequest count];
    else
        return [Farce count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (self.airTable==tableView) {
        static NSString *CellIdentifier = @"CellAir";
        

        AWAirCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell == nil) {
            cell = [[AWAirCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        
        cell.lblName.text = [self convertCodeToName:listRequest[indexPath.row][@"Code"]];
        cell.lblFarce.text=[[[self sortFarce:listRequest[indexPath.row][@"Fares"]] firstObject][@"TotalAmount"] stringValue];
         return cell;
    }else
     {
        static NSString *CellIdentifier = @"CellFares";
        
        
        UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell == nil) {
            cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        
        cell.textLabel.text = [Farce[indexPath.row][@"TotalAmount"] stringValue];
        return cell;
    }
 
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.airTable==tableView) {
    
        Farce=[self sortFarce:listRequest[indexPath.row][@"Fares"]];
        [self.faresTable reloadData];
    
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 0.0;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
   return 0.0;
}
-(NSArray*)sortFarce:(NSArray*)farce{
    
    farce = [farce sortedArrayUsingComparator:^NSComparisonResult(NSDictionary * f1, NSDictionary * f2 ) {
        
        return [f1[@"TotalAmount"] compare:f2[@"TotalAmount"] ];
    }];
    return  farce;
}



-(void)myProgressTask{

    float progress=0.0;
    while (progress < 100.0f) {
        progress=[[[AWSyncManager shared] apiGetProgress] floatValue];
        NSLog(@"%f",progress);
        self.navigationController.view.rn_activityView.progress = progress/100.0;
        usleep(10000);
    }
    
 
    listRequest= [[AWSyncManager shared] apiGetListRequest];

    [self.airTable reloadData];
    
    
}

-(NSString *)convertCodeToName:(NSString *) code{
    
    for (NSDictionary * item in airlineNames)
        if ([item[@"Code"] isEqualToString:code])
            return item[@"Name"];
    
    return code;
}

@end
