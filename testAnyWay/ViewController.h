//
//  ViewController.h
//  testAnyWay
//
//  Created by Admin on 07.04.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AWDataManager.h"
#import "CKCalendarView.h"
#import "AWSyncManager.h"

@interface ViewController : UIViewController<CKCalendarDelegate>

{
    
    BOOL isSearchCityFrom;
    BOOL isSearchCityTo;
    
    NSDictionary * cityFrom;
    NSDictionary * cityTo;
}

@property (weak, nonatomic) IBOutlet UITextField *tfCityFrom;
@property (weak, nonatomic) IBOutlet UITextField *tfCityTo;
@property (weak, nonatomic) IBOutlet UITextField *tfDate;

@property (weak, nonatomic) IBOutlet UIStepper *stepForAdult;
@property (weak, nonatomic) IBOutlet UIStepper *stepTeenager;
@property (weak, nonatomic) IBOutlet UIStepper *stepKid;

@property (weak, nonatomic) IBOutlet UITextField *tfAdultCount;
@property (weak, nonatomic) IBOutlet UITextField *tfTeenCount;
@property (weak, nonatomic) IBOutlet UITextField *tfKidCount;

@property (weak, nonatomic) IBOutlet UISwitch *switchEkonom;
@property (weak, nonatomic) IBOutlet UISwitch *switchBusiness;


@property(nonatomic, weak) CKCalendarView *calendar;
@property(nonatomic, strong) NSDateFormatter *dateFormatter;
@property(nonatomic, strong) NSDate *minimumDate;
@property(nonatomic, strong) UILabel *dateLabel;
@property(nonatomic, strong) NSArray *disabledDates;

@end

