//
//  AWDataManager.h
//  testAnyWay
//
//  Created by Admin on 07.04.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AWDataManager : NSObject{
    NSDictionary * listRequest;
}


+(AWDataManager *)shared;

@property (nonatomic,strong) NSDictionary * resultSearchCity;
@property (nonatomic,strong) NSDictionary * paramSearchBilet;
@property (nonatomic,strong) NSString * requestID;


@end
