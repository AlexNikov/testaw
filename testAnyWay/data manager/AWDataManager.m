//
//  AWDataManager.m
//  testAnyWay
//
//  Created by Admin on 07.04.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "AWDataManager.h"

@implementation AWDataManager

+(AWDataManager *)shared {
    
    static AWDataManager  * instance;
    
    @synchronized(self) {
        if(!instance) {
            instance = [[AWDataManager alloc] init];
            
        }
    }
    
    return instance;
}



@end
