//
//  AWCityDS.m
//  testAnyWay
//
//  Created by Admin on 08.04.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "AWCityDS.h"

@implementation AWCityDS


+(AWCityDS *)shared {
    
    static AWCityDS  * instance;
    
    @synchronized(self) {
        if(!instance) {
            instance = [[AWCityDS alloc] init];
            
        }
    }
    
    return instance;
}
-(void)update{
   
    NSLog(@"searchString=%@",self.searchString);
    NSPredicate * predicate = [NSPredicate predicateWithFormat:@"(City contains[cd] %@)", self.searchString];
    cityList = [[self.rawCityList[@"Array"] filteredArrayUsingPredicate:predicate] copy];
    
}

-(NSInteger)countCity{
    return  cityList.count;
}

-(NSDictionary*)cityByIndex:(NSInteger)index{
    return  cityList[index];
}

@end
