//
//  AWSearchCityVC.h
//  testAnyWay
//
//  Created by Admin on 07.04.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AWDataManager.h"
#import "AWSyncManager.h"
#import "AWCityDS.h"

@interface AWSearchCityVC : UITableViewController<UISearchBarDelegate,UIActionSheetDelegate>{
    NSString * oldSearchString;
    BOOL isDelCharInSearchString;
    BOOL isUpdateApiRequestCity;
    
    UIBarButtonItem *chooseLngBtn;
}

@end
