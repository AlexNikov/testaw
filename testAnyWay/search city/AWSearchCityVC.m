//
//  AWSearchCityVC.m
//  testAnyWay
//
//  Created by Admin on 07.04.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "AWSearchCityVC.h"

@interface AWSearchCityVC ()

@end

@implementation AWSearchCityVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    isUpdateApiRequestCity=NO;
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateTable)
                                                 name:@"updateSearchList"
                                               object:nil];
    NSString * lng=[[NSUserDefaults standardUserDefaults] objectForKey:@"searchLng"];
    
    if (lng==nil)
        lng=@"RU";
    
    
    chooseLngBtn = [[UIBarButtonItem alloc] initWithTitle:lng  style:UIBarButtonItemStyleDone target:self action:@selector(chooseLng)];
    
    self.navigationItem.rightBarButtonItem = chooseLngBtn;

}

-(void)chooseLng{
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"Выбирите язык поиска"
                                                             delegate:self
                                                    cancelButtonTitle:nil
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:nil];
    
    
        
        
    [actionSheet addButtonWithTitle:@"RU"];
    [actionSheet addButtonWithTitle:@"UK"];
    [actionSheet addButtonWithTitle:@"DE"];
    [actionSheet addButtonWithTitle:@"EN"];
    

    
    
    actionSheet.cancelButtonIndex = [actionSheet addButtonWithTitle:@"Отмена"];
    
    //actionSheet.tag = tag;
    
    [actionSheet showInView:self.view];
}

-(void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {

    if (buttonIndex==0) {
        chooseLngBtn.title=@"RU";
        [[NSUserDefaults standardUserDefaults] setObject:@"RU" forKey:@"searchLng"];
    }
    if (buttonIndex==1) {
        chooseLngBtn.title=@"UK";
        [[NSUserDefaults standardUserDefaults] setObject:@"UK" forKey:@"searchLng"];
    }
    if (buttonIndex==2) {
        chooseLngBtn.title=@"DE";
        [[NSUserDefaults standardUserDefaults] setObject:@"DE" forKey:@"searchLng"];
    }
    if (buttonIndex==3) {
        chooseLngBtn.title=@"EN";
        [[NSUserDefaults standardUserDefaults] setObject:@"EN" forKey:@"searchLng"];
    }

}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return [[AWCityDS shared] countCity];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    // Set the data for this cell:
    
    cell.textLabel.text = [[AWCityDS shared] cityByIndex:indexPath.row][@"City"];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
   
    [[AWDataManager shared] setResultSearchCity:[[AWCityDS shared] cityByIndex:indexPath.row] ];
    [self.navigationController popViewControllerAnimated:YES];
}




#pragma mark -
#pragma mark searchBarDelegate Method

-(BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar{
    
    return YES;
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    
    [[AWCityDS shared] setSearchString:searchText];
    
    //обновляю когда удаляю символ
    if (oldSearchString.length>searchText.length){
        [[AWCityDS shared] setRawCityList:[[AWSyncManager shared] apiSearchCity:searchText]];
        isDelCharInSearchString=YES;
    }
    
    
    if ((isDelCharInSearchString && oldSearchString.length<searchText.length && searchText.length>1) || (searchText.length>1 && !isUpdateApiRequestCity)) {
        [[AWCityDS shared] setRawCityList:[[AWSyncManager shared] apiSearchCity:searchText]];
        isUpdateApiRequestCity=YES;
        isDelCharInSearchString=NO;
    }
    
        
    oldSearchString=searchText;
    [self updateTable];
}


-(void)updateTable{
    [[AWCityDS shared] update];
    [self.tableView reloadData];
    
}


@end
