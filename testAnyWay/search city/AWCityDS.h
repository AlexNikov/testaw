//
//  AWCityDS.h
//  testAnyWay
//
//  Created by Admin on 08.04.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AWCityDS : NSObject{
    NSArray * cityList;
}

+(AWCityDS *)shared;
-(void)update;
-(NSInteger)countCity;
-(NSDictionary*)cityByIndex:(NSInteger)index;

@property(nonatomic,strong) NSDictionary * rawCityList;
@property(nonatomic,strong) NSString * searchString;

@end
