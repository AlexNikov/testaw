//
//  ViewController.m
//  testAnyWay
//
//  Created by Admin on 07.04.15.
//  Copyright (c) 2015 Admin. All rights reserved.
//

#import "ViewController.h"



@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    

}


-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    if ([[AWDataManager shared] resultSearchCity]!=nil && isSearchCityFrom) {
        cityFrom=[[AWDataManager shared] resultSearchCity];
        self.tfCityFrom.text=cityFrom[@"City"];
    }
    
    if ([[AWDataManager shared] resultSearchCity]!=nil && isSearchCityTo) {
        cityTo=[[AWDataManager shared] resultSearchCity];
        self.tfCityTo.text=cityTo[@"City"];
    }
    
    isSearchCityFrom=NO;
    isSearchCityTo=NO;
}
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{

    if([[segue identifier] isEqualToString:@"searchCityFrom"])
        isSearchCityFrom=YES;
    
    if([[segue identifier] isEqualToString:@"searchCityTo"])
        isSearchCityTo=YES;

    
}

-(BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender {
    if ([identifier isEqualToString:@"showBilet"]) {
        
        
        
        if (!self.switchBusiness.on && !self.switchEkonom.on){
            [[[UIAlertView alloc] initWithTitle:@"Ошибка" message:@"Укажите класс" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            
            return NO;
        }
        

        if (cityFrom==nil){
                    [[[UIAlertView alloc] initWithTitle:@"Ошибка" message:@"Не выбран пукт отпраления" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        
        return NO;
    }
        
        if (cityTo==nil){
            [[[UIAlertView alloc] initWithTitle:@"Ошибка" message:@"Не выбран пукт прибытия" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            
            return NO;
        }
        
        if (self.tfDate.text.length==0){
            [[[UIAlertView alloc] initWithTitle:@"Ошибка" message:@"Не выбрана дата" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            
            return NO;
        }
        
        int pass=self.stepForAdult.value+self.stepTeenager.value+self.stepKid.value;
        
        if (pass==0){
            [[[UIAlertView alloc] initWithTitle:@"Ошибка" message:@"Укажите пассажиров" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            
            return NO;
        }
        
        NSString * classFly=(self.switchBusiness.on && self.switchEkonom.on)?@"A":(self.switchBusiness.on)?@"B":@"E";
        NSString * reqStr=[NSString stringWithFormat:@"%@%@%@AD%dCN%dIN%dSC%@",self.tfDate.text,cityFrom[@"CityCode"],cityTo[@"CityCode"],(int)self.stepForAdult.value,(int)self.stepTeenager.value,(int)self.stepKid.value,classFly ];
        
        


        
        
        
        [[AWSyncManager shared] apiGetRequestID:reqStr];

    }
    return YES;
    
}

#pragma mark -
#pragma mark - UIAction

- (IBAction)getDate:(id)sender {
    
    CKCalendarView *calendar = [[CKCalendarView alloc] initWithStartDay:startMonday];
    self.calendar = calendar;
    calendar.delegate = self;
    
    self.dateFormatter = [[NSDateFormatter alloc] init];
    [self.dateFormatter setDateFormat:@"ddMM"];
    self.minimumDate = [NSDate date];
    
    
    calendar.onlyShowCurrentMonth = NO;
    calendar.adaptHeightToNumberOfWeeksInMonth = YES;
    
    calendar.center=self.view.center;
    [self.view addSubview:calendar];

    self.view.backgroundColor = [UIColor whiteColor];
    

}


- (IBAction)AdultChange:(UIStepper *)sender {
    
    if ((int)[sender value]>=6) 
        sender.value=6;
    

    self.tfAdultCount.text=[NSString stringWithFormat:@"%d",(int)[sender value]];
}

- (IBAction)teenChange:(UIStepper *)sender {
    if ((int)[sender value]>=6)
        sender.value=6;
    
    
    self.tfTeenCount.text=[NSString stringWithFormat:@"%d",(int)[sender value]];
}

- (IBAction)kidChange:(UIStepper *)sender {
    
    if ((int)[sender value]>=6)
        sender.value=6;
    
    
    self.tfKidCount.text=[NSString stringWithFormat:@"%d",(int)[sender value]];
}






#pragma mark -
#pragma mark - CKCalendarDelegate

- (void)calendar:(CKCalendarView *)calendar didSelectDate:(NSDate *)date {
    

    
    if ([[NSDate date] compare:date]==NSOrderedAscending || [self isSameDay:[NSDate date] otherDay:date]) {
        self.tfDate.text = [self.dateFormatter stringFromDate:date];
        for (UIView *subview in [self.view subviews]) {
            if (subview == self.calendar) {
                [subview removeFromSuperview];
            }
        }
        
    }else{
        
                    [[[UIAlertView alloc] initWithTitle:@"Ошибка" message:@"Неверная дата" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        
    }
    

}

- (BOOL)isSameDay:(NSDate*)date1 otherDay:(NSDate*)date2 {
    NSCalendar* calendar = [NSCalendar currentCalendar];
    
    unsigned unitFlags = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit;
    NSDateComponents* comp1 = [calendar components:unitFlags fromDate:date1];
    NSDateComponents* comp2 = [calendar components:unitFlags fromDate:date2];
    
    return [comp1 day]   == [comp2 day] &&
    [comp1 month] == [comp2 month] &&
    [comp1 year]  == [comp2 year];
}

@end
